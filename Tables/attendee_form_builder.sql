/*
-- Query: select * from ercess_db.attendee_form_builder order by event_id desc limit 70
-- Date: 2019-08-25 00:42
*/

CREATE TABLE `attendee_form_builder` (
  `ques_id` int(5) NOT NULL,
  `event_id` int(5) NOT NULL,
  `ques_title` mediumtext NOT NULL,
  `ques_type` int(2) NOT NULL,
  `ques_accessibility` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (89,36809,'Age',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (90,36809,'Age',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (91,36809,'Age',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (88,36808,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (87,36807,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (86,36806,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (85,36805,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (84,36804,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (83,36782,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (82,36768,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (81,36767,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (80,36766,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (79,36765,'proactive@rmmsolutions.com',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (78,36764,'patrick.arora@rmmsolutions.com',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (77,36763,'parteek26@gmail.com',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (76,36762,'abaisoya1@gmail.com',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (75,36761,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (74,36760,'abaisoya1@gmail.com',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (73,36759,'AJ.Fletcher@CWWC.org',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (72,36758,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (71,36757,'Age',6,2);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (70,36755,'Age',6,2);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (69,36749,'ds',1,2);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (68,36746,'date of birth',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (67,36738,'ddate',1,2);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (62,36736,'Gender',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (63,36736,'Date of Birth',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (64,36736,'Age',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (65,36736,'T-shirt size',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (66,36736,'Blood group',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (56,36729,'Birth Date',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (57,36729,'Gender',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (58,36729,'Emergency Contact Number',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (59,36729,'Blood Group',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (60,36729,'Address',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (61,36729,'Tshirt Size',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (55,36727,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (49,36699,'Gender',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (50,36699,'Date of Birth',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (51,36699,'Blood Group',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (52,36699,'Age Category',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (53,36699,'T-shirt Size',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (54,36699,'City',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (48,36644,'Company Nmae',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (47,36643,'Company Name',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (46,36642,'Company Name',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (45,36628,'Date of birth',7,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (43,36627,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (44,36627,'Gender',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (42,36623,'COmpany Nmae',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (41,36614,'Company Nmae',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (40,36613,'Company Name',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (38,36612,'dfegrt',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (39,36612,'8541',2,2);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (37,36586,'College Name',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (36,36579,'COLLEGE NAME',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (35,36578,'COLLEGE NAME',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (34,36577,'College Name',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (33,36571,'Company Name',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (32,36263,'College Name',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (31,36258,'Company Name',2,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (27,31924,'Age',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (28,31924,'Emergency Contact',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (29,31924,'Home Lacation',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (30,31924,'How did you come to know about the event',5,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (22,19556,'School/College/Institution From',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (23,19555,'Child\'s name',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (24,19555,'Child\'s age (in years)',6,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (25,19555,'Parents name',1,1);
INSERT INTO `attendee_form_builder` (`ques_id`,`event_id`,`ques_title`,`ques_type`,`ques_accessibility`) VALUES (20,4880,'School Name',1,2);

ALTER TABLE `attendee_form_builder`
  ADD PRIMARY KEY (`ques_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendee_form_builder`
--
ALTER TABLE `attendee_form_builder`
  MODIFY `ques_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
