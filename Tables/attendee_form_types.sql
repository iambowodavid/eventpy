/*
-- Query: select * from ercess_db.attendee_form_types
LIMIT 0, 1000

-- Date: 2019-08-25 00:43
*/

CREATE TABLE `attendee_form_types` (
  `type_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (1,'text');
INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (2,'paragraph');
INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (5,'dropdown');
INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (6,'number');
INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (7,'date');
INSERT INTO `attendee_form_types` (`type_id`,`name`) VALUES (8,'url');

ALTER TABLE `attendee_form_types`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendee_form_types`
--
ALTER TABLE `attendee_form_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
