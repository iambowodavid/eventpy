/*
-- Query: select * from ercess_db.partner_currencies
LIMIT 0, 1000

-- Date: 2019-08-25 00:50
*/

CREATE TABLE `partner_currencies` (
  `table_id` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `partner_id` int(11) NOT NULL,
  `currency_name` varchar(30) NOT NULL,
  `currency_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `partner_currencies` (`table_id`,`country`,`date_added`,`partner_id`,`currency_name`,`currency_id`) VALUES (7,'India','2019-06-08 13:22:07',2,'INR',1);
INSERT INTO `partner_currencies` (`table_id`,`country`,`date_added`,`partner_id`,`currency_name`,`currency_id`) VALUES (8,'India','2019-06-08 13:47:45',8,'INR',1);
INSERT INTO `partner_currencies` (`table_id`,`country`,`date_added`,`partner_id`,`currency_name`,`currency_id`) VALUES (9,'India','2019-06-08 13:49:25',14,'INR',NULL);
INSERT INTO `partner_currencies` (`table_id`,`country`,`date_added`,`partner_id`,`currency_name`,`currency_id`) VALUES (10,'India','2019-06-08 14:11:57',13,'USD',NULL);
INSERT INTO `partner_currencies` (`table_id`,`country`,`date_added`,`partner_id`,`currency_name`,`currency_id`) VALUES (11,'India','2019-06-29 18:11:57',1,'INR',NULL);

ALTER TABLE `partner_currencies`
  ADD PRIMARY KEY (`table_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `partner_currencies`
--
ALTER TABLE `partner_currencies`
  MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
