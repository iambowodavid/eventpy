/*
-- Query: select * from ercess_db.ercess_other_mappings
LIMIT 0, 1000

-- Date: 2019-08-25 00:50
*/

CREATE TABLE `ercess_other_mappings` (
  `table_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `id` int(10) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `ercess_equivalent` varchar(50) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (1,'India',1,'country','',47,'2019-08-01 18:07:41');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (2,'United Kingdom',2,'country','',47,'2019-08-01 18:07:41');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (3,'United States',3,'country','',47,'2019-08-01 18:09:21');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (4,'Australia',4,'country','',47,'2019-08-01 18:09:29');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (5,'Canada',5,'country','',47,'2019-08-01 18:09:44');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (6,'Andaman and Nicobar Islands',37,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (7,'Andhra Pradesh',24,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (8,'Arunachal Pradesh',44,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (9,'Assam',45,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (10,'Bihar',30,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (11,'Chandigarh',41,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (12,'Chhattisgarh',31,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (13,'Dadra and Nager Haveli',59,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (14,'Daman and Diu',58,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (15,'Goa',18,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (16,'Gujarat',28,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (17,'Haryana',32,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (18,'Himachal pradesh',33,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (19,'Jammu Kashmir',34,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (20,'Jharkhand',35,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (21,'Karnataka',23,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (22,'Kerala',1,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (23,'Lakshadweep',63,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (24,'Madhya Pradesh',36,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (25,'Maharashtra',60,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (26,'Manipur',46,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (27,'Meghalaya',47,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (28,'Mizoram',42,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (29,'Nagaland',48,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (30,'New Delhi',61,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (31,'Orissa',49,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (32,'Pondicherry',56,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (33,'Punjab',50,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (34,'Rajasthan',51,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (35,'Sikkim',52,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (36,'Tamil Nadu',2,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (37,'Telangana',62,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (38,'Tripura',53,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (39,'Uttar Pradesh',54,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (40,'Uttarakhand',57,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (41,'West Bengal',55,'state','',6,'2019-08-01 19:56:16');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (42,'Andaman &amp; Nicobar',17,'state','',37,'2019-08-13 09:15:30');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (43,'Andhra Pradesh',5,'state','',37,'2019-08-13 09:15:30');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (44,'Arunachal Pradesh',18,'state','',37,'2019-08-13 09:16:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (45,'Assam',19,'state','',37,'2019-08-13 09:16:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (46,'Chandigarh',20,'state','',37,'2019-08-13 09:17:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (47,'Bihar',6,'state','',37,'2019-08-13 09:17:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (48,'Chhattisgarh',21,'state','',37,'2019-08-13 09:20:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (49,'Dadra &amp; Nager Haveli',22,'state','',37,'2019-08-13 09:20:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (50,'Daman &amp; Diu',23,'state','',37,'2019-08-13 09:21:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (51,'Delhi',4,'state','',37,'2019-08-13 09:21:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (52,'Gujarat',1,'state','',37,'2019-08-13 09:22:00');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (53,'GOA',24,'state','',37,'2019-08-13 09:22:00');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (54,'Haryana',25,'state','',37,'2019-08-13 09:22:39');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (55,'Himachal Pradesh',26,'state','',37,'2019-08-13 09:22:39');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (56,'Jammu &amp; Kashmir',27,'state','',37,'2019-08-13 09:23:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (57,'Jharkhand',28,'state','',37,'2019-08-13 09:23:24');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (58,'Karnataka',15,'state','',37,'2019-08-13 09:24:03');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (59,'Kerala',29,'state','',37,'2019-08-13 09:24:03');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (60,'Lakshadweep',30,'state','',37,'2019-08-13 09:26:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (61,'Madhya Pradesh',3,'state','',37,'2019-08-13 09:26:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (62,'Maharastra',2,'state','',37,'2019-08-13 09:26:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (63,'Manipur',31,'state','',37,'2019-08-13 09:26:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (64,'Meghalaya',32,'state','',37,'2019-08-13 09:27:13');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (65,'Mizoram',33,'state','',37,'2019-08-13 09:27:13');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (66,'Nagaland',34,'state','',37,'2019-08-13 09:27:49');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (67,'Orissa',35,'state','',37,'2019-08-13 09:27:49');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (68,'Pondicherry',36,'state','',37,'2019-08-13 09:28:25');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (69,'Punjab',37,'state','',37,'2019-08-13 09:28:25');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (70,'Rajasthan',38,'state','',37,'2019-08-13 09:28:57');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (71,'Sikkim',39,'state','',37,'2019-08-13 09:28:57');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (72,'Tamil Nadu',14,'state','',37,'2019-08-13 09:29:43');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (73,'Telangana',43,'state','',37,'2019-08-13 09:29:43');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (74,'Tripura',40,'state','',37,'2019-08-13 09:30:25');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (75,'Uttar Pradesh',41,'state','',37,'2019-08-13 09:30:25');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (76,'Uttaranchal',42,'state','',37,'2019-08-13 09:31:21');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (77,'West Bengal',16,'state','',37,'2019-08-13 09:31:21');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (78,'Andaman Nicobar',1,'state','',34,'2019-08-13 11:53:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (79,'Andhra Pradesh',2,'state','',34,'2019-08-13 11:53:17');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (80,'Arunachal Pradesh',3,'state','',34,'2019-08-13 11:54:50');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (81,'Assam',4,'state','',34,'2019-08-13 11:54:50');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (82,'Bihar',5,'state','',34,'2019-08-13 11:56:28');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (83,'Chandigarh',6,'state','',34,'2019-08-13 11:56:28');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (84,'Chhattisgarh',7,'state','',34,'2019-08-13 11:56:28');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (85,'Dadra &amp; Nagar Haveli ',8,'state','',34,'2019-08-13 11:56:28');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (86,'Daman &amp; Diu',9,'state','',34,'2019-08-13 11:56:28');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (87,'Delhi',10,'state','',34,'2019-08-13 11:57:30');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (88,'Goa',11,'state','',34,'2019-08-13 11:57:30');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (89,'Gujarat',12,'state','',34,'2019-08-13 12:05:01');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (90,'Haryana',13,'state','',34,'2019-08-13 12:05:01');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (91,'Himachal Pradesh',14,'state','',34,'2019-08-13 12:05:01');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (92,'Jammu &amp; Kashmir',15,'state','',34,'2019-08-13 12:05:01');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (93,'Jharkhand',16,'state','',34,'2019-08-13 12:05:01');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (94,'Karnataka',17,'state','',34,'2019-08-13 12:05:39');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (95,'Kerala',18,'state','',34,'2019-08-13 12:05:39');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (96,'Ladakh',38,'state','',34,'2019-08-13 12:07:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (97,'Lakshadweep',19,'state','',34,'2019-08-13 12:07:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (98,'Madhya Pradesh',20,'state','',34,'2019-08-13 12:07:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (99,'Maharashtra',21,'state','',34,'2019-08-13 12:07:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (100,'Manipur',22,'state','',34,'2019-08-13 12:07:06');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (101,'Meghalaya',23,'state','',34,'2019-08-13 12:08:23');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (102,'Mizoram',24,'state','',34,'2019-08-13 12:08:23');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (103,'Nagaland',25,'state','',34,'2019-08-13 12:08:23');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (104,'Odisha',26,'state','',34,'2019-08-13 12:08:23');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (105,'Puducherry',27,'state','',34,'2019-08-13 12:08:23');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (106,'Punjab',28,'state','',34,'2019-08-13 12:10:33');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (107,'Rajasthan',29,'state','',34,'2019-08-13 12:10:33');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (108,'Sikkim',30,'state','',34,'2019-08-13 12:10:33');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (109,'Tamil Nadu',31,'state','',34,'2019-08-13 12:10:33');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (110,'Telangana',37,'state','',34,'2019-08-13 12:10:33');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (111,'Tripura',32,'state','',34,'2019-08-13 12:11:51');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (112,'Uttar Pradesh',33,'state','',34,'2019-08-13 12:11:51');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (113,'Uttarakhand',34,'state','',34,'2019-08-13 12:11:51');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (114,'West Bengal',35,'state','',34,'2019-08-13 12:11:51');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (115,'India',1,'country','India',25,'2019-08-19 03:11:51');
INSERT INTO `ercess_other_mappings` (`table_id`,`name`,`id`,`type`,`ercess_equivalent`,`partner_id`,`date_added`) VALUES (116,'United States',3,'country','United States',25,'2019-08-19 03:11:51');

ALTER TABLE `ercess_other_mappings`
  ADD PRIMARY KEY (`table_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ercess_other_mappings`
--
ALTER TABLE `ercess_other_mappings`
  MODIFY `table_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
