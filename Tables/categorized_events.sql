/*
-- Query: select * from ercess_db.categorized_events order by event_id desc limit 70
-- Date: 2019-08-25 00:41
*/
CREATE TABLE `categorized_events` (
  `table_id` int(9) NOT NULL,
  `event_id` int(9) NOT NULL,
  `category_id` int(4) NOT NULL,
  `topic_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22264,36813,21,NULL);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22263,36810,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22262,36809,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22261,36808,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22260,36807,31,125);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22259,36806,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22258,36805,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22257,36804,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22256,36803,24,82);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22255,36800,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22254,36799,31,125);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22253,36798,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22252,36797,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22251,36796,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22250,36795,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22249,36794,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22248,36793,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22247,36792,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22246,36788,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22245,36786,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22244,36784,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22243,36783,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22242,36782,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22241,36776,31,125);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22240,36775,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22239,36774,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22238,36773,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22237,36772,2,5);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22236,36771,11,30);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22235,36770,11,31);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22234,36769,32,110);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22233,36768,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22232,36767,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22231,36766,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22230,36765,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22229,36764,6,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22228,36763,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22227,36762,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22226,36761,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22225,36760,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22224,36759,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22223,36758,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22222,36757,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22221,36756,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22220,36755,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22219,36754,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22218,36750,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22217,36749,10,26);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22216,36748,19,56);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22215,36747,19,53);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22214,36746,8,21);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22213,36745,31,123);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22212,36744,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22211,36743,31,125);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22210,36742,31,125);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22209,36741,7,NULL);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22208,36740,19,54);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22207,36739,9,24);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22206,36738,36,NULL);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22205,36737,6,18);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22204,36736,23,64);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22203,36735,31,124);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22202,36729,23,64);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22201,36728,23,64);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22200,36727,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22199,36725,2,117);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22198,36723,16,44);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22197,36722,19,54);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22196,36721,19,52);
INSERT INTO `categorized_events` (`table_id`,`event_id`,`category_id`,`topic_id`) VALUES (22195,36720,19,52);

ALTER TABLE `categorized_events`
  ADD PRIMARY KEY (`table_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorized_events`
--
ALTER TABLE `categorized_events`
  MODIFY `table_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22235;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
