/*
-- Query: select * from ercess_db.partner_timezones
LIMIT 0, 1000

-- Date: 2019-08-25 00:49
*/

CREATE TABLE `partner_timezones` (
  `timezone_id` int(11) NOT NULL,
  `country` varchar(40) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `partner_id` int(11) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `partner_zone_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (1,'India','2019-06-08 13:07:24',2,'India Standard Time',1);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (2,'Singapore','2019-06-08 13:07:24',2,'Singapore Time',2);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (3,'India','2019-06-08 13:50:38',14,'Indian Standard Time',327);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (4,'India','2019-06-08 14:13:50',13,'Asia/Calcutta',NULL);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (5,'India','2019-06-29 17:35:50',1,'Asia/Kolkata',NULL);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (6,'India','2019-07-29 17:35:50',16,'(GMT+05:30) Kolkata',NULL);
INSERT INTO `partner_timezones` (`timezone_id`,`country`,`last_updated`,`partner_id`,`timezone`,`partner_zone_id`) VALUES (7,'India','2019-07-29 17:35:50',51,'India Standard Time',NULL);

ALTER TABLE `partner_timezones`
  ADD PRIMARY KEY (`timezone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `partner_timezones`
--
ALTER TABLE `partner_timezones`
  MODIFY `timezone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
