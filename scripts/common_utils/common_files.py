from datetime import datetime
from mysql.connector import pooling
import os


DBUser =os.environ['DBUser']
DBPass = os.environ['DBPassword']


connection_pool = pooling.MySQLConnectionPool(pool_name="pynative_pool",pool_size=20,pool_reset_session=True,
                                              host='localhost',database='ercess_db',user=DBUser,password=DBPass)

def get_conn():
    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()
    return connection_object, cursor

def events_write_process():

    connection_object, cursor = get_conn()

    sql1 = """SELECT event_status_on_channel.event_id, partner_sites.table_id, partner_sites.site_name, event_status_on_channel.table_id
            FROM event_status_on_channel
            INNER JOIN partner_sites
            ON event_status_on_channel.site_id = partner_sites.table_id
            WHERE event_status_on_channel.promotion_status = 'ready to upload' AND partner_sites.method = 'manual'"""
    try:
        cursor.execute(sql1)
        data1 = cursor.fetchall()
        f = open('event_details.txt', 'w')
        for info1 in data1:
            eventid = info1[0]
            siteid = info1[1]
            sitename = info1[2]
            tableid = info1[3]
            res = (str(eventid)+','+str(siteid)+','+str(sitename)+','+str(tableid)+'\n')
            f.write(res)

    except Exception as e:
        print('Something went wrong while fetching from event_status_on_channel')
        print(e)


    cursor.close()
    connection_object.close()



def main_process(event_id,site_id,response=None,index=None):

    connection_object, cursor = get_conn()
    contact_number = 9886188705
    email_id = 'promotion@ercess.com'
    organization_name = 'Ercess Live'
    main_json = {}
    users_list = []
    topics_list = []
    tickets_list = []
    articles2_list = []
    ticket_discount_list = []
    attendee_form_options_list = []
    ercess_partners_categories_list = []
    ercess_partners_subcategories_list = []
    status_promotion_ticketing_list = []
    other_details_list = []
    ercess_partners_more_list = []
    var_currency_name = ''
    var_currency_id = ''
    var_timezone_name = ''
    var_timezone_id = ''
    partner_email1 = ''
    partner_email2 = ''
    partner_cc = ''
    link = ''

    # event website
    sql0 = """SELECT event_status_on_channel.link FROM event_status_on_channel WHERE link != '' and link != ' ' and link != 'None' and promotion_status='published' and partner_status='approved' and event_id ='%d' LIMIT 1""" %event_id
    try:
        cursor.execute(sql0)
        data0 = cursor.fetchall()
        for info0 in data0:
            link = info0[0]

    except Exception as e:
        print('Something went wrong while fetching link from event_status_on_channel table.')
        print(e)


    sql = """SELECT articles2.id, articles2.country, articles2.event_name, articles2.profile_image,
        articles2.banner, articles2.sDate, articles2.eDate, articles2.address_line1,
        articles2.address_line2, articles2.pincode, articles2.state, articles2.city, articles2.full_address,
        articles2.latitude, articles2.longitude, articles2.start_time, articles2.end_time,
        articles2.description, articles2.website
        FROM articles2 WHERE id ='%d'""" %event_id
    try:

        cursor.execute(sql)
        data = cursor.fetchall()
        for info in data:
            eventid = info[0]
            countryname = info[1]
            eventname = info[2]
            profileimg = info[3]
            banner0 = info[4]
            sdate = str(info[5])[:10]
            edate = str(info[6])[:10]
            addr1 = info[7]
            addr2 = info[8]
            pincode = info[9]
            state = info[10]
            city = info[11]
            full_address = info[12]
            latitude = info[13]
            longitude = info[14]
            stime = str(info[15])

            s_time = datetime.strptime(stime,"%H:%M:%S")
            stime = s_time.strftime("%I:%M:%S %p")

            etime = str(info[16])

            e_time = datetime.strptime(etime,"%H:%M:%S")
            etime = e_time.strftime("%I:%M:%S %p")

            description = info[17]
            website = info[18]

            articles2 = {"event id":eventid, "country":countryname, "event name":eventname,
            "profile image":profileimg, "banner":banner0, "start date":sdate, "end date":edate,
            "address 1":addr1, "address 2":addr2, "pincode":pincode, "state":state, "city":city,
            "full address":full_address, "latitude":latitude, "longitude":longitude,"start time":stime,
            "end time":etime, "description":description, "website": website, "existing event url": link}
            articles2_list.append(articles2)

        main_json['event info'] = articles2_list
        # print(main_json['event info'])
    except Exception as e:
        print('Something went wrong while fetching from articles2 table.')
        print(e)


    #tickets
    # import ipdb; ipdb.set_trace()
    sql1 = """SELECT * FROM tickets WHERE event_id = '%d' and active = 1""" %event_id
    tkt_qty_sum = 0
    try:
        cursor.execute(sql1)
        data1 = cursor.fetchall()
        for info1 in data1:
            tktid = info1[0]
            eventid1 = info1[1]
            tktname = info1[2]
            original_tkt_price = info1[3]
            other_charges = info1[4]
            other_charges_type = info1[5]
            tkt_qty = info1[6]
            min_qty = info1[7]
            max_qty = info1[8]
            qty_left = info1[9]
            ticket_msg = info1[10]
            if other_charges == '':
                other_charges = 0


            ticket_start_date = str(info1[11])[:10]

            ticket_start_time = str(info1[11])[11:]


            expiry_date = str(info1[12])[:10]
            expiry_time = str(info1[12])[11:]

            ticket_label= info1[13]
            active1 = info1[14]

            tkt_qty_sum+= tkt_qty
            if other_charges_type == 1:
                if other_charges == 0:
                    tkt_price = str(int(original_tkt_price) + int(other_charges))
                else:
                    tkt_price = str(int(original_tkt_price))
            elif other_charges_type == 0:
                tkt_price = str(int(original_tkt_price))
            elif other_charges_type == 2:
                if other_charges == 0:
                    tkt_price = str(int(original_tkt_price) * (1 + (int(other_charges) / 100)))
                else:
                    tkt_price = str(int(original_tkt_price))
            else:
                tkt_price = str(int(original_tkt_price))


            if other_charges_type == 1:
                other_charges_type = 'flat'
            elif other_charges_type == 2:
                other_charges_type = 'percent'

            if active1 == 1:
                active1 = 'active'
            else:
                active1 = 'inactive'

            tickets ={"ticket id": tktid, "ticket name":tktname, "original ticket price":original_tkt_price,"ticket price":tkt_price,"other charges":other_charges,"other charges type":other_charges_type,
                      "ticket quantity":tkt_qty, "minimum quantity":min_qty, "maximum quantity":max_qty, "quantity left":qty_left, "ticket message":ticket_msg,
                      "ticket start date":ticket_start_date, "ticket start time": ticket_start_time, "expiry date":expiry_date, "expiry time": expiry_time, "ticket label":ticket_label, "active":active1}
            tickets_list.append(tickets)

        tickets_list.append({'capacity': tkt_qty_sum})
        main_json['tickets'] = tickets_list
        # print(main_json['tickets'])

    except Exception as e:
        print('Something went wrong while fetching from tickets table.')
        print(e)


    #categories of events
    sql3 = """SELECT categorized_events.category_id, categorized_events.topic_id FROM categorized_events WHERE categorized_events.event_id = '%d'""" %event_id
    try:
        cursor.execute(sql3)
        data3 = cursor.fetchall()
        for info3 in data3:
            category_id = info3[0]
            topic_id = info3[1]

    except Exception as e:
        print('Something went wrong while fetching from categorized_events table.')
        print(e)


    #status promotion ticketing
    sql5 = """SELECT * FROM status_promotion_ticketing WHERE event_id ='%d'""" %event_id
    try:
        cursor.execute(sql5)
        data5 = cursor.fetchall()
        for info5 in data5:
            sptkt_id = info5[0]
            eventid5 = info5[1]
            private5 = info5[4]
            ticketing = info5[8]
            connected_user = info5[10]

            if private5 == 0:
                private5 = 'public'
                privateRow = 0
                publicRaw = 1
            elif private5 == 1:
                private5 = 'private'
                privateRow = 1
                publicRaw = 0

            if ticketing == 0:
                ticketing = 'free'
                ticketingRaw = 0
            elif ticketing == 1:
                ticketing = 'paid'
                ticketingRaw = 1

            status_promotion_ticketing = {"private":private5, "private raw":privateRow, "public raw":publicRaw, "ticketing":ticketing, "ticketing raw":ticketingRaw, "connected user":connected_user}
            status_promotion_ticketing_list.append(status_promotion_ticketing)

        main_json['status promotion ticketing'] = status_promotion_ticketing_list
    except Exception as e:
        print('Something went wrong while fetching from status_promotion_ticketing table.')
        print(e)

    # attendees
    sql6="""SELECT * FROM attendee_form_builder WHERE event_id ='%d'""" %event_id
    try:
        json_6=[]######################
        cursor.execute(sql6)
        data6 = cursor.fetchall()
        for info6 in data6:
            qts_id6 = info6[0]
            qts_title6 = info6[2]
            qts_type6 = info6[3]
            qts_acces6 = info6[4]

            if qts_acces6 == 1:
                qts_acces6 = 'mandatory'
            elif qts_acces6 == 2:
                qts_acces6 = 'optional'

            json_6.append({"question id":qts_id6, "question title":qts_title6, "question type":qts_type6, "question accessibility":qts_acces6})#############
            

            sql7 ="""SELECT * FROM `attendee_form_types` WHERE type_id = '%d'""" %qts_type6
            try:
                cursor.execute(sql7)
                data7 = cursor.fetchall()
                for info7 in data7:
                    # name7 = info7[1] ##############

                    # json_6['name'] = name7 ##########

                    sql8 = """ SELECT * FROM `attendee_form_options` WHERE event_id = '%d' AND ques_id = '%d'""" %(event_id,qts_id6)
                    # print(sql8)
                    try:
                        cursor.execute(sql8)
                        data8 = cursor.fetchall()
                        if data8 == None:#####################
                            attendee_form_options_list.append({})##################

                        for info8 in data8:
                            option_id8 = info8[0]
                            option_name8 = info8[3]

                            attendee_form_options = {'option id':option_id8, "option name":option_name8}
                            attendee_form_options_list.append(attendee_form_options)
                    except Exception as e:
                        print('Something went wrong while fetching from attendee_form_options table.')
                        print(e)
                json_6[-1]['options']=attendee_form_options_list#################

            except Exception as e:
                print('Something went wrong while fetching from attendee_form_types table.')
                print(e)

        main_json['question']=json_6####################
        # main_json['attendees'] = attendee_form_options_list###################
    except Exception as e:
        print('Something went wrong while fetching from attendee_form_builder table.')
        print(e)


    # users
    sql9 = """SELECT * FROM users WHERE id ='%d'""" %connected_user
    try:
        cursor.execute(sql9)
        data9 = cursor.fetchall()
        for info9 in data9:
            frstnme9 = info9[3]
            lastnme9 = info9[4]
            user9 = info9[5]
            mobile9 = info9[6]

            users ={'first name': frstnme9, 'last name': lastnme9, 'user': user9, 'mobile no': mobile9}
            users_list.append(users)

        main_json['users'] = users_list
    except Exception as e:
        print('Something went wrong while fetching from users table.')
        print(e)


    # ercess partners categories
    sql10 = """SELECT ercess_partners_categories.ercess_category, ercess_partners_categories.partner_category, ercess_partners_categories.partner_category_id
                    FROM ercess_partners_categories WHERE ercess_category = '%d' and partner_id = '%d'""" %(category_id, site_id)
    try:
        cursor.execute(sql10)
        data10 = cursor.fetchall()
        for info10 in data10:
            ercess_catgry = info10[0]
            partner_catgry = info10[1]
            partner_category_id = info10[2]

            ercess_partners_categories = {'ercess category': ercess_catgry, 'partner category':partner_catgry, 'partner category id':partner_category_id}
            ercess_partners_categories_list.append(ercess_partners_categories)
            print(ercess_partners_categories_list)
        main_json['ercess partners categories'] = ercess_partners_categories_list
    except Exception as e:
        print('Something went wrong while fetching from ercess_partners_categories table.')
        print(e)

    # ercess partners sub categories
    sql11 = """SELECT ercess_partners_subcategories.partner_subcate_name, ercess_partners_subcategories.partner_subcate_id
                    FROM ercess_partners_subcategories WHERE ercess_topic_id = '%d' and partner_id = '%d'""" %(topic_id, site_id)
    try:
        cursor.execute(sql11)
        data11 = cursor.fetchall()
        for info11 in data11:
            partner_subcategory_name = info11[0]
            partner_subcategory_id = info11[1]

            ercess_partners_subcategories = {'partner subcategory':partner_subcategory_name, 'partner subcategory id':partner_subcategory_id}
            ercess_partners_subcategories_list.append(ercess_partners_subcategories)
            print(ercess_partners_subcategories_list)
        main_json['ercess partners subcategories'] = ercess_partners_subcategories_list
    except Exception as e:
        print('Something went wrong while fetching from ercess_partners_subcategories table.')
        print(e)


    # partner's details
    sql12 = """SELECT partner_sites.email1, partner_sites.email2, partner_sites.cc
                    FROM partner_sites WHERE country = '%s' and table_id = '%d'""" %(countryname, site_id)
    try:
        cursor.execute(sql12)
        data12 = cursor.fetchall()
        for info12 in data12:
            partner_email1 = info12[0]
            partner_email2 = info12[1]
            partner_cc = info12[2]
    except Exception as e:
        print('Something went wrong while fetching from partner_timezones table.')
        print(e)


    # partner's timezone
    sql13 = """SELECT partner_timezones.timezone, partner_timezones.partner_zone_id
                    FROM partner_timezones WHERE country = '%s' and partner_id = '%d'""" %(countryname, site_id)
    try:
        cursor.execute(sql13)
        data13 = cursor.fetchall()
        for info13 in data13:
            var_timezone_name = info13[0]
            var_timezone_id = info13[1]
    except Exception as e:
        print('Something went wrong while fetching from partner_timezones table.')
        print(e)


    # partner's currency
    sql14 = """SELECT partner_currencies.currency_name, partner_currencies.currency_id
                    FROM partner_currencies WHERE country = '%s' and partner_id = '%d'""" %(countryname, site_id)
    try:
        cursor.execute(sql14)
        data14 = cursor.fetchall()
        for info14 in data14:
            var_currency_name = info14[0]
            var_currency_id = info14[1]
    except Exception as e:
        print('Something went wrong while fetching from partner_currencies table.')
        print(e)

    other_details = {'timezone name': var_timezone_name, 'timezone id': var_timezone_id,
        'currency name': var_currency_name, 'currency id': var_currency_id,
        'contact number': contact_number, 'email id': email_id, 'organization name': organization_name,
        'partner email1': partner_email1, 'partner email1': partner_email2, 'partner cc': partner_cc}
    other_details_list.append(other_details)
    main_json['other details'] = other_details_list

    

    sql15 = """SELECT ercess_other_mappings.name, ercess_other_mappings.id, ercess_other_mappings.type, ercess_other_mappings.ercess_equivalent 
                    FROM ercess_other_mappings WHERE partner_id = '%d'""" %(site_id)
    try:
        cursor.execute(sql15)
        data15 = cursor.fetchall()
        for info15 in data15:
            var_name = info15[0]
            var_id = info15[1]
            var_type = info15[2]
            var_equivalent = info15[3]

            ercess_partners_more = {'name':var_name, 'id':var_id, 'type':var_type, 'ercess equivalent': var_equivalent}
            ercess_partners_more_list.append(ercess_partners_more)
            print(ercess_partners_more_list)
        main_json['ercess partners more mappings'] = ercess_partners_more_list
        print(main_json['ercess partners more mappings'])
    except Exception as e:
        print('Something went wrong while fetching from ercess_other_mappings table.')
        print(e)
    

    cursor.close()
    connection_object.close()
##    ##print(main_json)


    if not (response and index) == None:
        response[index] = main_json
    else:
        return main_json


def respective_sites_event_details():

    events_write_process()
    event_maxima = open('../event_details.txt', 'w+')
    with open('event_details.txt', 'r') as f:
        event_details = f.readlines()

    for i in event_details:
        temp = i.replace('\n', '')
        temp = tuple(temp.split(','))
        if temp[1] == '10':
            res = (str(temp[0]) + ',' + str(temp[1]) + ',' + str(temp[2]) + '\n')
            event_maxima.write(res)

    event_maxima.close()

respective_sites_event_details()
