import mysql.connector
from mysql.connector import Error
from mysql.connector.connection import MySQLConnection
from mysql.connector import pooling
import os

DBUser = os.environ['DBUser']
DBPass = os.environ['DBPassword']
connection_pool = pooling.MySQLConnectionPool(pool_name="pynative_pool",pool_size=20,pool_reset_session=True,host='localhost',database='ercess_db',user=DBUser,password=DBPass)

def get_conn():
    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()
    return connection_object, cursor
