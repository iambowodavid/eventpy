#!/usr/bin/python3
from datetime import datetime
from common_utils.common_files import main_process
from website import login, post_event
import pickle



with open('event_details.txt', 'r') as f:
    events = [x.strip().split(',') for x in f.read().splitlines() if 'highape' in x]

data = []
for event in events:
    print(event)
    data.append(main_process(int(event[0]), int(event[1])))


session = login()
for d in data:
    post_event(session, d,events[data.index(d)][3])

