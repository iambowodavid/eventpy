from dbconnection import get_conn
import sys

##url = sys.argv[1]
##tbl_id = sys.argv[2]
##promotion_status = sys.argv[3]
##partner_status = sys.argv[4]
##connection_object, cursor = get_conn()
##updateDb(url,tbl_id,promotion_status,partner_status)
def updateDb(url,tbl_id,promotion_status,partner_status):

    connection_object, cursor = get_conn()
    sql = """UPDATE event_status_on_channel 
            SET link = '%s', promotion_status = '%s', partner_status = '%s'
            WHERE table_id = %s"""%(url, promotion_status, partner_status, tbl_id)

    cursor.execute(sql)
    connection_object.commit()

    connection_object.close()
    cursor.close()