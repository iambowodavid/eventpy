import os

os.environ["DBUser"] = ""
os.environ["DBPassword"] = ""
os.environ["siteEmail"] = ""
os.environ["sitePassword"] = ""

from requests_toolbelt.multipart.encoder import MultipartEncoder
from email.mime.multipart import MIMEMultipart
from datetime import datetime, timedelta
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from update_db import updateDb
from bs4 import BeautifulSoup
from dateparser import parse
import urllib.parse
import traceback
import requests
import smtplib
import json
import sys



def exception_to_string(excp):
   stack =  traceback.extract_tb(excp.__traceback__)  
   pretty = traceback.format_list(stack)
   return ''.join(pretty) + '\n  {} {}'.format(excp.__class__,excp)

def report_error(error,eventId,eventName):
    emailId  = infoEmail
    password = infoPassword
    s = smtplib.SMTP(host='smtp.gmail.com', port =587)
    s.starttls()
    s.login(emailId, password)


    msg = MIMEMultipart()   

    msg['From']=emailId
    msg['To']=""
    msg['Subject']="event creation failed"
    
    msg.attach(MIMEText(error, 'plain'))
    
    s.send_message(msg)
    # del msg
        

    # s.quit()


def login():
    requestSession = requests.session()
    response = requestSession.get('https://partners.highape.com/login')
    markup = response.text
    bSoup = BeautifulSoup(markup, "html.parser")
    
    #get the anti cross-site-request-forgery token
    csrfToken = bSoup.find_all(attrs={"name": "_token"}) 
    #check to make sure csrf token exists; terminate with error message if it not
    csrfToken = checkSoupElement(csrfToken, "Unable to fetch csrf token", response.url) 

    print("About to login.........")
    data={
        "_token": csrfToken,
        "email": os.environ["siteEmail"] ,
        "password": os.environ["sitePassword"] 
    }

    response = requestSession.post('https://partners.highape.com/login',data=data)
    markup = response.text
    bSoup = BeautifulSoup(markup, "html.parser")

    #check to make sure user is logged in
    userID = bSoup.find_all(id="user_id")
    #terminate with error message if not
    checkSoupElement(userID, "Unable to verify that user is logged, most likely caused by user authentication failure", response.url) 

    #now that we know user is logged in
    print("Successfully logged in\nAttempting to create all events")

    post_event(requestSession, response)
      
def post_event(requestSession, response):
    try:
        bSoup = BeautifulSoup(response.text, "html.parser")
        csrfToken = bSoup.find_all(attrs={"name": "_token"}) 
        csrfToken = checkSoupElement(csrfToken, "Unable to fetch csrf token", response.url) 

        for eventData in getAllEventsData():
             eventData.update({"_token": csrfToken})
             multipart_data = MultipartEncoder(eventData)
             headers={
                 "Content-Type": multipart_data.content_type,
             }
             response = requestSession.post('https://partners.highape.com/create_event_new', data=multipart_data, headers=headers)
        
             saveData(response.text, "data.txt.html")
             print("check data.txt.html")
    except Exception as e:
        print(e)
        #report_error(exception_to_string(e),event_id,event_name)

################## -- get all events data helper - retrieves all events data from the database-- #################
def getAllEventsData():
    eventList = [ {
   "_token":"bPhpDqYCGvQmVuKTYcXvLsPgAHz9UatDlOeeqTT6",
   "event_name":"hello",
   "event_venue":"Aloft Bengaluru Whitefield\u00a0-\u00a0Whitefield\u00a0-\u00a0Bangalore",
   "venue_id":"436",
   "venue_city":"Bangalore",
   "venue_address":"17, Sadaramangala Road, Thigalarapalya, Hoodi, Sadara Mangala Industrial Area, Thigalarapalya, Krishnarajapura, Bengaluru, Karnataka 560066, India",
   "start_date":"08\/30\/2019",
   "start_time":"01:00 AM",
   "end_date":"08\/31\/2019",
   "end_time":"01:00 AM",
   "occurrence":"0",
   "multiple_date":"",
   "about_desc_editor":"<p>rr<\/p>",
   "image_data":"62588eeebddf5c85c27441b095bdbcd0event_image.jpg",
   "youtube_url":"",
   "fileToUpload" : ('1＊3qwjNjfD0STfGCDjiPL5hQ.jpg',  open("event_image.jpg", 'rb'), 'image/jpg'),
   "pro-image" : ('',  '', ''),
   "category[]": "3",
   "artist_performer": "",
   "special_details": "",
   "tc_editor": "",
   "event_type": "2",
   "ticket_name[0]": "Stags",
   "ticket_name[1]": "Ladies",
   "ticket_name[2]": "Couples",
   "ticket_tickets[0]": "500",
   "ticket_tickets[1]": "500",
   "ticket_tickets[2]": "500",
   "ticket_cost[0]": "0",
   "ticket_cost[1]": "0",
   "ticket_cost[2]": "0",
   "ticket_desc[0]": "Each pass allows entry to 1 Stag.",
   "ticket_desc[1]": "Each pass allows entry to 1 Lady.",
   "ticket_desc[2]": "Each pass allows entry to 1 Couple.",
   "coupon_name[]": "",
   "coupon_type[]": "0",
   "coupon_discount[]": "",
   "coupon_status[]": "1",
   "booking": "1"
   } ]

    return  eventList

################## -- save data helper -- #################
def saveData(data, fileName):
    outF = open(fileName, "w")
    outF.writelines(data)
    outF.close()

################## -- element checker - checks to make sure an element exists before use -- #################
def checkSoupElement(element, errorMSG, url):
    if element is None or len(element) < 1 or element[0].get("value") is None:
        sys.exit("Fatal ERROR: " + errorMSG + "\nError occurr while processing the page at: " + url + "\nTerminating script.......")
    else:
        return element[0].get("value")

if __name__=='__main__':
    login()